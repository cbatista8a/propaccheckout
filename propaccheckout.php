<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Propaccheckout extends Module
{
    protected $config_form = false;
    public $import_table_customer = '';
    public $import_table_giacenze = '';
    public $import_table_conai = '';
    protected $html = '';
    protected $operation_logs = '';

    public function __construct()
    {
        $this->name = 'propaccheckout';
        $this->tab = 'migration_tools';
        $this->version = '1.0.0';
        $this->author = 'OrangePix Srl';
        $this->need_instance = 1;
        $this->import_table_customer = _DB_PREFIX_.'orangeimport_customer';
        $this->import_table_giacenze = _DB_PREFIX_.'orangeimport_giacenze';
        $this->import_table_conai = _DB_PREFIX_.'conai_values';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Propac Checkout | OrangePix Srl');
        $this->description = $this->l('Custom Checkout');

        $this->confirmUninstall = $this->l('This action deactivates the classes that this module has modified.');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');
        $this->installTemplatesOverrides($this->local_path.'templates_overrides/');
        $this->installTemplatesOverrides($this->local_path.'templates_overrides/','tpl');
        $this->installTemplatesOverrides($this->local_path.'templates_overrides/','twig');
        Tools::clearAllCache();

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayCheckoutSubtotalDetails') &&
            $this->registerHook('displayCalcDefaultTotals') &&
            $this->registerHook('displayUpdateSummaries') &&
            $this->registerHook('displayValuesOnly') &&
            $this->registerHook('displayPropacProductTabContent');
    }

    public function uninstall()
    {
        include(dirname(__FILE__).'/sql/uninstall.php');

        //Delete override Files
        $this->DeleteOverrideFiles();
        $this->rollbackTemplatesOverrides($this->local_path.'templates_overrides/');
        $this->rollbackTemplatesOverrides($this->local_path.'templates_overrides/','tpl');
        $this->rollbackTemplatesOverrides($this->local_path.'templates_overrides/','twig');
        Tools::clearAllCache();
        parent::uninstall();

        return true;
    }


    /**
     * Install and override files on proyect folders
     * @param        $directory
     * @param string $ext
     */
    public function installTemplatesOverrides($directory, $ext='php'){
        foreach (Tools::scandir($directory, $ext, '', true) as $file) {
            $name = pathinfo($file)['filename'];
            $original_file = _PS_ROOT_DIR_.'/'.$file;
            if ($this->rename_file($original_file,$name.'.backup',false) || !file_exists($original_file))
                copy($directory.$file,$original_file);
        }
    }


    /**
     * Rollback Installed files on proyect folders
     * @param        $directory
     * @param string $ext
     */
    public function rollbackTemplatesOverrides($directory, $ext='php'){
        foreach (Tools::scandir($directory, $ext, '', true) as $file) {
            $name = pathinfo($file)['filename'];
            $path = pathinfo($file)['dirname'];
            $original_name = pathinfo($file)['basename'];
            $override_file = _PS_ROOT_DIR_.'/'.$file;
            $backup_file= _PS_ROOT_DIR_.'/'.$path.'/'.$name.'.backup';
            if (file_exists($backup_file)){
                unlink($override_file);
                $this->rename_file($backup_file,$original_name,false);
            }
        }
    }

    /**
     * Rename file
     * @param string    $old_path
     * @param string    $name
     * @param bool      $keep_ext Preserve original Extension or not
     *
     * @return bool
     */
    public function rename_file($old_path, $name,$keep_ext=true)
    {
        if (file_exists($old_path)) {
            $info=pathinfo($old_path);
            $new_path=$info['dirname']."/".($keep_ext ? $name.".".$info['extension'] : $name);
            if (file_exists($new_path)) {
                return false;
            }
            return rename($old_path, $new_path);
        }
        return false;
    }

    /**
     * Scan files in ThisModule/override/ Folder
     * @param $directory
     *
     * @return array
     */
    public function ScanOverrideFiles($directory){
        $files = array();
        foreach (Tools::scandir($directory, 'php', '', true) as $file) {
            $files[] = $file;
        }
        return $files;
    }


    /**
     * Delete files used by this module in rootProyect/override/ Folder
     * @return bool
     */
    public function DeleteOverrideFiles(){
            $success=true;
            //Register Override Files for Uninstall
            $directory = $this->local_path.'override/';
            $overrides = json_encode($this->ScanOverrideFiles($directory)) ;
            $overrides = str_replace('[','',$overrides);
            $overrides = str_replace(']','',$overrides);
            $overrides = explode(',',stripslashes($overrides));

            foreach ($overrides as $file){
                $file =  trim($file,"\"");
                if (file_exists(_PS_OVERRIDE_DIR_.$file) && !is_dir(_PS_OVERRIDE_DIR_.$file)){
                    try {
                        unlink(_PS_OVERRIDE_DIR_.$file);
                    }catch (Exception $e){
                        $this->displayError($e->getMessage());
                        $success = false;
                    }
                }
            }
        return $success;

    }

    /**
     * Load the configuration form
     *
     * @throws PrestaShopException
     */
    public function getContent()
    {
        $this->html = '';
        $this->operation_logs = '';
        /**
         * If values have been submitted in the form, process.
         */

        if (((bool)Tools::isSubmit('import')) == true) {

        }

        $this->context->smarty->assign('module_dir', $this->_path);
        $output = $this->html;
        $output .= $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output;
    }


    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {

    }

    /**
     * For display Conai field in Product Combination Page (backoffice)
     * @param $params
     *
     * @return string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookDisplayPropacProductTabContent($params)
    {
        $attribute = new Combination((int)$params['id_product_attribute']);
        $id_attribute = $params['id_product_attribute'];
        $html= '<div class="col-md-3">
                        <fieldset class="form-group">
                            <label class="form-control-label">Conai</label>
                            <div class="input-group">
                                <input type="text" id="combination_'.$id_attribute.'_attribute_conai" name="combination_'.$id_attribute.'[attribute_conai]" class="conai form-control" value="'.$attribute->conai.'">
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-3">
                        <fieldset class="form-group">
                            <label class="form-control-label">Quantity per pack</label>
                            <div class="input-group">
                                <input type="text" id="combination_'.$id_attribute.'_attribute_quantity_per_pack" name="combination_'.$id_attribute.'[attribute_quantity_per_pack]" class="conai form-control" value="'.$attribute->quantity_per_pack.'">
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-3">
                        <fieldset class="form-group">
                            <label class="form-control-label">Pallet</label>
                            <div class="input-group">
                                <input type="text" id="combination_'.$id_attribute.'_attribute_pallet" name="combination_'.$id_attribute.'[attribute_pallet]" class="pallet form-control" value="'.$attribute->pallet.'">
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-3">
                        <fieldset class="form-group">
                            <label class="form-control-label">Sconto Pallet</label>
                            <div class="input-group">
                                <input type="text" id="combination_'.$id_attribute.'_attribute_sconto_pallet" name="combination_'.$id_attribute.'[attribute_sconto_pallet]" class="pallet form-control" value="'.$attribute->sconto_pallet.'">
                            </div>
                        </fieldset>
                    </div>';
        return $html;
    }



    /**
     * Display Totals in theme templates without Taxs
     * @param $params
     *
     * @return false|string
     */
    public function hookDisplayCalcDefaultTotals($params)
    {
        //verifica quando nei parametri non c'é il carrello ma l'ordine si
        if (null === $params['cart'] && !empty($params['order'])){
            $cart = new Cart(is_object($params['order'])?$params['order']->id_cart:$params['order']['id_cart']);
        }
        elseif(((bool)Tools::isSubmit('controller') == true && Tools::getValue('controller') == 'orderconfirmation')){
            $cart = new Cart(Tools::getValue('id_cart'));
        }
        elseif(((bool)Tools::isSubmit('controller') == true && Tools::getValue('controller') == 'pdfinvoice')){
            $cart = new Cart(is_object($params['order'])?$params['order']->id_cart:$params['order']['id_cart']);
        }
        else{
            $cart = is_object($params['cart']) ? $params['cart'] : $this->context->cart;
        }
        return json_encode($this->getTotalsWithOutGroupIva($cart));
    }


    /**
     * Display html rows Conai and IvaGroup in Modal template
     * @param $params
     *
     * @return string|void
     */
    public function hookDisplayCheckoutSubtotalDetails($params)
    {
        if (((bool)Tools::isSubmit('action') == true) && Tools::getValue('action') === 'add-to-cart') {
            $html = '';
            $cart = is_object($params['cart']) ? $params['cart'] : $this->context->cart;

            $result =$this->getTotalsWithOutGroupIva($cart);
            if ($this->convertFormatPriceToFloat($result['conai']) > 0){
                $html .= '<p><strong>' . $this->l('Conai') . ':</strong>&nbsp;' . $result['conai'] . '</p>';
                if ($this->convertFormatPriceToFloat($result['sconto_conai']) > 0){
                    $html .= '<p><strong>' . $this->l('Sconto sul Conai') . ':</strong>&nbsp;-' . $result['sconto_conai'] . '</p>';
                }
            }
            if ($this->convertFormatPriceToFloat($result['total_iva']) > 0)
                $html .= '<p><strong>' . $this->l('IVA') . ':</strong>&nbsp;' . $result['total_iva'] . '</p>';
            return $html;
        }
        return;
    }

    /**
     * Display html rows Conai and IvaGroup in theme templates
     * @param $params
     *
     * @return string
     */
    public function hookDisplayUpdateSummaries($params)
    {
        $html = '';
        $cart = is_object($params['cart']) ? $params['cart'] : $this->context->cart;
        $result =$this->getTotalsWithOutGroupIva($cart);
        if ($this->convertFormatPriceToFloat($result['conai']) > 0) {
            $html .= '<div class="cart-summary-line" id="cart-subtotal-conai">
                          <span class="label">
                                          ' . $this->l('Conai') . '
                          </span>
                          <span class="value">' . $result['conai'] . '</span>
                        </div>';
            if ($this->convertFormatPriceToFloat($result['sconto_conai']) > 0){
                $html .= '<div class="cart-summary-line" id="cart-subtotal-sconto_conai">
                          <span class="label">
                                          ' . $this->l('Sconto sul Conai') . '
                          </span>
                          <span class="value">-' . $result['sconto_conai'] . '</span>
                        </div>';
            }
        }
        if ($this->convertFormatPriceToFloat($result['discounts']) > 0){
            $html .= '<div class="cart-summary-line" id="cart-subtotal-discounts">
                          <span class="label">
                                          '.$this->l('Sconti').'
                          </span>
                          <span class="value">-'.$result['discounts'].'</span>
                        </div><br>';
        }
        if ($this->convertFormatPriceToFloat($result['total_iva']) > 0){
            $html .= '<div class="cart-summary-line" id="cart-subtotal-iva-group">
                          <span class="label">
                                          '.$this->l('IVA').'
                          </span>
                          <span class="value">'.$result['total_iva'].'</span>
                        </div>';
        }

        return $html;
    }

    public function convertFormatPriceToFloat($price=''){
        $price = str_replace('€','',$price);
        $price = str_replace(',','.',$price);
        return (float)$price;
    }

    /**
     * Calc totals without Taxs
     * @param Cart $cart
     *
     * @return array
     */
    public function getTotalsWithOutGroupIva($cart)
    {
        $response = array();
        $products_in_cart = $cart->getProducts();
        $conai = Cart::getConaiTotal($products_in_cart,false,false);
        $conai_with_discount = Cart::getConaiTotal($products_in_cart,true,false);
        $productsAndShipping = $cart->getOrderTotal(false,Cart::BOTH, null, null, false);
        $productsAndShipping_with_iva = $cart->getOrderTotal(true,Cart::BOTH, null, null, false);
        $total_iva = abs($productsAndShipping_with_iva - $productsAndShipping);
        $products_without_iva = $cart->getOrderTotal(false,Cart::ONLY_PRODUCTS);
        $shipping = $cart->getOrderTotal(false,Cart::ONLY_SHIPPING);
        $discounts = $cart->getTotalDiscountCustom();

        $currency = is_array($this->context->currency) ? $this->context->currency['iso_code'] : $this->context->currency->iso_code;
        $response['products_real'] = $products_without_iva > 0 ? $this->context->getCurrentLocale()->formatPrice(Tools::ps_round($products_without_iva, _PS_PRICE_COMPUTE_PRECISION_),$currency) : 0;
        $response['conai'] = $conai_with_discount > 0 ? $this->context->getCurrentLocale()->formatPrice(Tools::ps_round($conai_with_discount, _PS_PRICE_COMPUTE_PRECISION_),$currency) : 0;
        $response['sconto_conai'] = ($conai - $conai_with_discount) > 0 ? $this->context->getCurrentLocale()->formatPrice( Tools::ps_round(($conai - $conai_with_discount), _PS_PRICE_COMPUTE_PRECISION_),$currency) : 0;
        $response['total_iva'] = ($total_iva) > 0 ? $this->context->getCurrentLocale()->formatPrice(Tools::ps_round($total_iva, _PS_PRICE_COMPUTE_PRECISION_),$currency) : 0;
        $response['shipping_real'] = $shipping > 0 ? $this->context->getCurrentLocale()->formatPrice( Tools::ps_round($shipping, _PS_PRICE_COMPUTE_PRECISION_),$currency) : 'Gratis';
        $response['discounts'] = $discounts > 0 ? $this->context->getCurrentLocale()->formatPrice( Tools::ps_round($discounts, _PS_PRICE_COMPUTE_PRECISION_),$currency) : 0;
        return $response;
    }

}