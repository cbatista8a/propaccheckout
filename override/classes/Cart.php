<?php
/**
 * 2007-2018 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

use PrestaShop\PrestaShop\Core\Cart\Calculator;
class Cart extends CartCore
{
    /**
     * @param bool $withTaxes
     * @param int $type
     * @param null $products
     * @param null $id_carrier
     * @param bool $use_cache
     * @return float|int
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function getOrderTotal(
        $withTaxes = true,
        $type = Cart::BOTH,
        $products = null,
        $id_carrier = null,
        $use_cache = false
    ) {
        if ((int) $id_carrier <= 0) {
            $id_carrier = null;
        }
        if ($type == Cart::ONLY_PRODUCTS_WITHOUT_SHIPPING) {
            $type = Cart::ONLY_PRODUCTS;
        }
        $type = (int) $type;
        $allowedTypes = array(
            Cart::ONLY_PRODUCTS,
            Cart::ONLY_DISCOUNTS,
            Cart::BOTH,
            Cart::BOTH_WITHOUT_SHIPPING,
            Cart::ONLY_SHIPPING,
            Cart::ONLY_WRAPPING,
            Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING,
        );
        if (!in_array($type, $allowedTypes)) {
            throw new \Exception('Invalid calculation type: ' . $type);
        }
        if ($type == Cart::ONLY_DISCOUNTS && !CartRule::isFeatureActive()) {
            return 0;
        }
        $virtual = $this->isVirtualCart();
        if ($virtual && $type == Cart::ONLY_SHIPPING) {
            return 0;
        }
        if ($virtual && $type == Cart::BOTH) {
            $type = Cart::BOTH_WITHOUT_SHIPPING;
        }
        if (null === $products) {
            $products = $this->getProducts();
        }
        if ($type == Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING) {
            foreach ($products as $key => $product) {
                if ($product['is_virtual']) {
                    unset($products[$key]);
                }
            }
            $type = Cart::ONLY_PRODUCTS;
        }
        if (Tax::excludeTaxeOption()) {
            $withTaxes = false;
        }
        $cartRules = array();
        if (in_array($type, [Cart::BOTH, Cart::BOTH_WITHOUT_SHIPPING, Cart::ONLY_DISCOUNTS])) {
            $cartRules = $this->getTotalCalculationCartRules($type, $type == Cart::BOTH);
        }
        $calculator = $this->newCalculator($products, $cartRules, $id_carrier);
        $computePrecision = $this->configuration->get('_PS_PRICE_COMPUTE_PRECISION_');
        switch ($type) {
            case Cart::ONLY_SHIPPING:
                $calculator->calculateRows();
                $calculator->calculateFees($computePrecision);
                $amount = $calculator->getFees()->getInitialShippingFees();
                break;
            case Cart::ONLY_WRAPPING:
                $calculator->calculateRows();
                $calculator->calculateFees($computePrecision);
                $amount = $calculator->getFees()->getInitialWrappingFees();
                break;
            case Cart::BOTH:
                $calculator->processCalculation($computePrecision);
                $amount = $calculator->getTotal();
                break;
            case Cart::BOTH_WITHOUT_SHIPPING:
                $calculator->calculateRows();
                $calculator->calculateCartRulesWithoutFreeShipping();
                $amount = $calculator->getTotal(true);
                break;
            case Cart::ONLY_PRODUCTS:
                $calculator->calculateRows();
                $amount = $calculator->getRowTotal();
                break;
            case Cart::ONLY_DISCOUNTS:
                $calculator->processCalculation($computePrecision);
                $amount = $calculator->getDiscountTotal();
                break;
            default:
                throw new \Exception('unknown cart calculation type : ' . $type);
        }
        $value = $withTaxes ? $amount->getTaxIncluded() : $amount->getTaxExcluded();
        if($type === Cart::BOTH) {
            $value = $this->getCartAmountCustom($withTaxes);
        }

        $compute_precision = $this->configuration->get('_PS_PRICE_COMPUTE_PRECISION_');
        return Tools::ps_round($value, $compute_precision);
    }


    /**
     * @param bool $withTaxes
     * @param bool $withDiscounts
     * @return float|int
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function getCartAmountCustom($withTaxes = true, $withDiscounts = true)
    {
        $total_amount_wt = 0;
        if (empty($this->_products)){
            return $total_amount_wt;
        }
        $is_free_shipping = false;
        $product_min_price_id = $this->getMinPriceIdByCartProducts();
        foreach ($this->_products as $product){
            $price = Product::getPriceStatic($product['id_product'],false,$product['id_product_attribute'],_PS_PRICE_COMPUTE_PRECISION_,null,false,true,$product['quantity']);
            if ($withDiscounts){
                $price = $this->applyDiscountByProductId($price * $product['quantity'] , $product['id_product'],$product_min_price_id,$product['id_product_attribute'], $product['quantity'],$is_free_shipping);
            }
            $iva_group = Group::getCurrent()->aliquota_iva;
            if ($withTaxes){
                if ($iva_group < $product['rate']){ //Applica l'IVA del Grupo Utente se è minore di quello del prodotto (Esempio se un utente è IVA essento)
                    Cart::applyGroupIva($price);
                }else{
                    $price *= (1 + ($product['rate'] / 100)); //Iva del prodotto
                }
            }
            $total_amount_wt += $withDiscounts ? $price : $price * $product['quantity'];
        }
        $shipping = $is_free_shipping ? 0 : $this->getOrderTotal($withTaxes,Cart::ONLY_SHIPPING);
        $wrappingFees = $this->getOrderTotal($withTaxes,Cart::ONLY_WRAPPING);
        $totalConai = self::getConaiTotal($this->getProducts(),$withDiscounts,$withTaxes);
        return ($total_amount_wt + $shipping + $wrappingFees + $totalConai);
    }

    /**
     * @param $price
     * @param $id_product
     * @param $product_min_price_id
     * @param $id_product_attribute
     * @param int $quantity
     * @param bool $is_free_shipping
     * @return float|int|mixed|string
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function applyDiscountByProductId($price, $id_product, $product_min_price_id, $id_product_attribute, $quantity = 1, &$is_free_shipping = false){
        $discount = 0;
        $cartRules = $this->getCartRules(CartRule::FILTER_ACTION_REDUCTION, false);
        foreach ($cartRules as $rule){
            $cart_rule_obj = new CartRule($rule['id_cart_rule']);
            if ($cart_rule_obj->checkValidity(Context::getContext(),true,false)){
                if ($rule['reduction_percent'] > 0){
                    $discount = $price * ($rule['reduction_percent']/100);
                }elseif ($rule['reduction_amount'] > 0){
                    $discount = $rule['reduction_amount'];
                }

                if (($rule['reduction_product'] == -1) && $id_product == $product_min_price_id){ //Prodotto meno caro
                    if ($cart_rule_obj->free_shipping){
                        $is_free_shipping = true;
                        $price -= $rule['value_tax_exc'] * $quantity;
                    }else{
                        $price -= $cart_rule_obj->getContextualValue(false) * $quantity;
                    }
                }
                elseif ($rule['product_restriction'] == 0 && $rule['reduction_product'] == 0){ // ordine senza spedizione
                    $price -= $rule['reduction_amount'] > 0 ? ($discount/count($this->_products)) : $discount;
                }elseif ($rule['product_restriction'] == 1 && ($rule['reduction_product'] == $id_product || $rule['reduction_product'] == -2)){ // product restrictions (category etc)
                    $matching_product = $id_product . '-' . $id_product_attribute;
                    $specific_products = $cart_rule_obj->checkProductRestrictionsFromCart($this,true);
                    if (in_array($matching_product, $specific_products, true)){
                        $price -= $rule['reduction_amount'] > 0 ? ($discount/count($specific_products)) : $discount;
                    }
                }

                if ($cart_rule_obj->free_shipping) {
                    $is_free_shipping = true;
                }

            }else{
                $this->removeCartRule($cart_rule_obj->id);
            }
        }
        return $price;
    }

    /**
     * @return float|int
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function getTotalDiscountCustom(){
        $total_cart_with_discounts = $this->getCartAmountCustom(false,true);
        $total_discounts = ($this->getCartAmountCustom(false,false) - $total_cart_with_discounts);
        return $total_discounts > 0 ? Tools::ps_round($total_discounts,_PS_PRICE_COMPUTE_PRECISION_) : 0;
    }

    /**
     * @return mixed
     */
    public function getMinPriceIdByCartProducts(){
        $product_id = $this->_products[0]['id_product'];
        $value = $this->_products[0]['price'];
        foreach ($this->_products as $product){
            if ($value > $product['price']){
                $value = $product['price'];
                $product_id = $product['id_product'];
            }
        }
        return $product_id;
    }

    /**
     * @param $products
     * @param bool $with_discount
     * @param bool $with_iva
     * @return float|int
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public static function getConaiTotal($products,$with_discount = true,$with_iva = true){
        $totalConai = 0;
        foreach ($products as $singleProduct) {
            $combination = new Combination($singleProduct['id_product_attribute']);
            $singleProductConai = $combination->getConai() * $singleProduct['cart_quantity'];
            if ($with_iva){
                $iva_group = Group::getCurrent()->aliquota_iva;
                if ($iva_group < $singleProduct['rate']){ //Applica l'IVA del Grupo Utente se è minore di quello del prodotto (Esempio se un utente è IVA essento)
                    Cart::applyGroupIva($singleProductConai);
                }else{
                    $singleProductConai *= (1 + ($singleProduct['rate'] / 100)); //Iva del prodotto
                }
            }
            $totalConai += $singleProductConai;
        }
        if ($with_discount){
            $customer = (self::$_customer) ? self::$_customer : Context::getContext()->customer;
            $sconto_conai = $customer->getScontoConai();
            $totalConai -= ($totalConai * $sconto_conai/100);
        }
        return Tools::ps_round($totalConai, _PS_PRICE_COMPUTE_PRECISION_);
    }
    /**
     * @param $value
     */
    public static function applyGroupIva(&$value){
        $DefaultGroup = Group::getCurrent();
        $group_iva = $DefaultGroup->aliquota_iva;
        $value = $value  * (1 + $group_iva/100);
    }

	/**
	 * Return package shipping cost.
	 *
	 * @param int $id_carrier Carrier ID (default : current carrier)
	 * @param bool $use_tax
	 * @param Country|null $default_country
	 * @param array|null $product_list list of product concerned by the shipping.
	 *                                 If null, all the product of the cart are used to calculate the shipping cost
	 * @param int|null $id_zone Zone ID
	 *
	 * @return float|bool Shipping total, false if not possible to ship with the given carrier
	 */
    public function getPackageShippingCost($id_carrier = null, $use_tax = true, Country $default_country = null, $product_list = null, $id_zone = null)
    {
        if ($this->isVirtualCart()) {
            return 0;
        }
        if (!$default_country) {
            $default_country = Context::getContext()->country;
        }
        if (null !== $product_list) {
            foreach ($product_list as $key => $value) {
                if ($value['is_virtual'] == 1) {
                    unset($product_list[$key]);
                }
            }
        }
        if (null === $product_list) {
            $products = $this->getProducts(false, false, null, true);
        } else {
            $products = $product_list;
        }
        if (Configuration::get('PS_TAX_ADDRESS_TYPE') == 'id_address_invoice') {
            $address_id = (int) $this->id_address_invoice;
        } elseif (is_array($product_list) && count($product_list)) {
            $prod = current($product_list);
            $address_id = (int) $prod['id_address_delivery'];
        } else {
            $address_id = null;
        }
        if (!Address::addressExists($address_id)) {
            $address_id = null;
        }
        if (null === $id_carrier && !empty($this->id_carrier)) {
            $id_carrier = (int) $this->id_carrier;
        }
        $cache_id = 'getPackageShippingCost_' . (int) $this->id . '_' . (int) $address_id . '_' . (int) $id_carrier . '_' . (int) $use_tax . '_' . (int) $default_country->id . '_' . (int) $id_zone;
        if ($products) {
            foreach ($products as $product) {
                $cache_id .= '_' . (int) $product['id_product'] . '_' . (int) $product['id_product_attribute'];
            }
        }
        if (Cache::isStored($cache_id)) {
            return Cache::retrieve($cache_id);
        }
        $order_total = $this->getOrderTotal(false, Cart::ONLY_PRODUCTS, $product_list);
        $shipping_cost = 0;
        if (!count($products)) {
            Cache::store($cache_id, $shipping_cost);
            return $shipping_cost;
        }
        if (!isset($id_zone)) {
            if (!$this->isMultiAddressDelivery()
                && isset($this->id_address_delivery) // Be carefull, id_address_delivery is not usefull one 1.5
                && $this->id_address_delivery
                && Customer::customerHasAddress($this->id_customer, $this->id_address_delivery)
            ) {
                $id_zone = Address::getZoneById((int) $this->id_address_delivery);
            } else {
                if (!Validate::isLoadedObject($default_country)) {
                    $default_country = new Country(Configuration::get('PS_COUNTRY_DEFAULT'), Configuration::get('PS_LANG_DEFAULT'));
                }
                $id_zone = (int) $default_country->id_zone;
            }
        }
        if ($id_carrier && !$this->isCarrierInRange((int) $id_carrier, (int) $id_zone)) {
            $id_carrier = '';
        }
        if (empty($id_carrier) && $this->isCarrierInRange((int) Configuration::get('PS_CARRIER_DEFAULT'), (int) $id_zone)) {
            $id_carrier = (int) Configuration::get('PS_CARRIER_DEFAULT');
        }
        $total_package_without_shipping_tax_inc = $this->getOrderTotal(true, Cart::ONLY_PRODUCTS, $product_list);
        if (empty($id_carrier)) {
            if ((int) $this->id_customer) {
                $customer = new Customer((int) $this->id_customer);
                $result = Carrier::getCarriers((int) Configuration::get('PS_LANG_DEFAULT'), true, false, (int) $id_zone, $customer->getGroups());
                unset($customer);
            } else {
                $result = Carrier::getCarriers((int) Configuration::get('PS_LANG_DEFAULT'), true, false, (int) $id_zone);
            }
            foreach ($result as $k => $row) {
                if ($row['id_carrier'] == Configuration::get('PS_CARRIER_DEFAULT')) {
                    continue;
                }
                if (!isset(self::$_carriers[$row['id_carrier']])) {
                    self::$_carriers[$row['id_carrier']] = new Carrier((int) $row['id_carrier']);
                }

                $carrier = self::$_carriers[$row['id_carrier']];
                $shipping_method = $carrier->getShippingMethod();
                if (($shipping_method == Carrier::SHIPPING_METHOD_WEIGHT && $carrier->getMaxDeliveryPriceByWeight((int) $id_zone) === false)
                    || ($shipping_method == Carrier::SHIPPING_METHOD_PRICE && $carrier->getMaxDeliveryPriceByPrice((int) $id_zone) === false)) {
                    unset($result[$k]);
                    continue;
                }
                if ($row['range_behavior']) {
                    $check_delivery_price_by_weight = Carrier::checkDeliveryPriceByWeight($row['id_carrier'], $this->getTotalWeight(), (int) $id_zone);
                    $total_order = $total_package_without_shipping_tax_inc;
                    $check_delivery_price_by_price = Carrier::checkDeliveryPriceByPrice($row['id_carrier'], $total_order, (int) $id_zone, (int) $this->id_currency);
                    if (($shipping_method == Carrier::SHIPPING_METHOD_WEIGHT && !$check_delivery_price_by_weight)
                        || ($shipping_method == Carrier::SHIPPING_METHOD_PRICE && !$check_delivery_price_by_price)) {
                        unset($result[$k]);
                        continue;
                    }
                }
                if ($shipping_method == Carrier::SHIPPING_METHOD_WEIGHT) {
                    $shipping = $carrier->getDeliveryPriceByWeight($this->getTotalWeight($product_list), (int) $id_zone);
                } else {
                    $shipping = $carrier->getDeliveryPriceByPrice($order_total, (int) $id_zone, (int) $this->id_currency);
                }
                if (!isset($min_shipping_price)) {
                    $min_shipping_price = $shipping;
                }
                if ($shipping <= $min_shipping_price) {
                    $id_carrier = (int) $row['id_carrier'];
                    $min_shipping_price = $shipping;
                }
            }
        }
        if (empty($id_carrier)) {
            $id_carrier = Configuration::get('PS_CARRIER_DEFAULT');
        }
        if (!isset(self::$_carriers[$id_carrier])) {
            self::$_carriers[$id_carrier] = new Carrier((int) $id_carrier, Configuration::get('PS_LANG_DEFAULT'));
        }
        $carrier = self::$_carriers[$id_carrier];
        if (!Validate::isLoadedObject($carrier)) {
            Cache::store($cache_id, 0);
            return 0;
        }
        $shipping_method = $carrier->getShippingMethod();
        if (!$carrier->active) {
            Cache::store($cache_id, $shipping_cost);
            return $shipping_cost;
        }
        if ($carrier->is_free == 1) {
            Cache::store($cache_id, 0);
            return 0;
        }
        if ($use_tax && !Tax::excludeTaxeOption()) {
            $address = Address::initialize((int) $address_id);
            if (Configuration::get('PS_ATCP_SHIPWRAP')) {
                $carrier_tax = 0;
            } else {
                $carrier_tax = $carrier->getTaxesRate($address);
            }
        }
        $configuration = Configuration::getMultiple(array(
            'PS_SHIPPING_FREE_PRICE',
            'PS_SHIPPING_HANDLING',
            'PS_SHIPPING_METHOD',
            'PS_SHIPPING_FREE_WEIGHT',
        ));
        $free_fees_price = 0;
        if (isset($configuration['PS_SHIPPING_FREE_PRICE'])) {
            $free_fees_price = Tools::convertPrice((float) $configuration['PS_SHIPPING_FREE_PRICE'], Currency::getCurrencyInstance((int) $this->id_currency));
        }
        $orderTotalwithDiscounts = $this->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING, null, null, false);
        if ($orderTotalwithDiscounts >= (float) ($free_fees_price) && (float) ($free_fees_price) > 0) {
            $shipping_cost = $this->getPackageShippingCostFromModule($carrier, $shipping_cost, $products);
            Cache::store($cache_id, $shipping_cost);
            return $shipping_cost;
        }
        if (isset($configuration['PS_SHIPPING_FREE_WEIGHT'])
            && $this->getTotalWeight() >= (float) $configuration['PS_SHIPPING_FREE_WEIGHT']
            && (float) $configuration['PS_SHIPPING_FREE_WEIGHT'] > 0) {
            $shipping_cost = $this->getPackageShippingCostFromModule($carrier, $shipping_cost, $products);
            Cache::store($cache_id, $shipping_cost);
            return $shipping_cost;
        }
        if ($carrier->range_behavior) {
            if (!isset($id_zone)) {
                if (isset($this->id_address_delivery)
                    && $this->id_address_delivery
                    && Customer::customerHasAddress($this->id_customer, $this->id_address_delivery)) {
                    $id_zone = Address::getZoneById((int) $this->id_address_delivery);
                } else {
                    $id_zone = (int) $default_country->id_zone;
                }
            }
            if (($shipping_method == Carrier::SHIPPING_METHOD_WEIGHT && !Carrier::checkDeliveryPriceByWeight($carrier->id, $this->getTotalWeight(), (int) $id_zone))
                || (
                    $shipping_method == Carrier::SHIPPING_METHOD_PRICE && !Carrier::checkDeliveryPriceByPrice($carrier->id, $total_package_without_shipping_tax_inc, $id_zone, (int) $this->id_currency)
                )) {
                $shipping_cost += 0;
            } else {
                if ($shipping_method == Carrier::SHIPPING_METHOD_WEIGHT) {
                    $shipping_cost += $carrier->getDeliveryPriceByWeight($this->getTotalWeight($product_list), $id_zone);
                } else { // by price
                    $shipping_cost += $carrier->getDeliveryPriceByPrice($order_total, $id_zone, (int) $this->id_currency);
                }
            }
        } else {
            if ($shipping_method == Carrier::SHIPPING_METHOD_WEIGHT) {
                $shipping_cost += $carrier->getDeliveryPriceByWeight($this->getTotalWeight($product_list), $id_zone);
            } else {
                $shipping_cost += $carrier->getDeliveryPriceByPrice($order_total, $id_zone, (int) $this->id_currency);
            }
        }
        if (isset($configuration['PS_SHIPPING_HANDLING']) && $carrier->shipping_handling) {
            $shipping_cost += (float) $configuration['PS_SHIPPING_HANDLING'];
        }
        foreach ($products as $product) {
            if (!$product['is_virtual']) {
                $shipping_cost += $product['additional_shipping_cost'] * $product['cart_quantity'];
            }
        }
        $shipping_cost = Tools::convertPrice($shipping_cost, Currency::getCurrencyInstance((int) $this->id_currency));
        $shipping_cost = $this->getPackageShippingCostFromModule($carrier, $shipping_cost, $products);
        if ($shipping_cost === false) {
            Cache::store($cache_id, false);
            return false;
        }
        if (Configuration::get('PS_ATCP_SHIPWRAP')) {
            if (!$use_tax) {
                $shipping_cost /= (1 + $this->getAverageProductsTaxRate());
            }
        } else {
            if ($use_tax && isset($carrier_tax)) {
                $shipping_cost *= 1 + ($carrier_tax / 100);
            }
        }
        $shipping_cost = (float) Tools::ps_round((float) $shipping_cost, 2);
        Cache::store($cache_id, $shipping_cost);
        return $shipping_cost;
    }
}
