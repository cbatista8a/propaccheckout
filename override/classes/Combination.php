<?php
/**
 * 2007-2018 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

/**
 * Class CombinationCore.
 */
class Combination extends CombinationCore
{


    public $conai = 0;
    public $pallet = 0;
    public $sconto_pallet = 0;
    public $quantity_per_pack = 1;
    public $combination_name = '';

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
      'table' => 'product_attribute',
      'primary' => 'id_product_attribute',
      'fields' => array(
        'id_product' => array('type' => self::TYPE_INT, 'shop' => 'both', 'validate' => 'isUnsignedId', 'required' => true),
        'location' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 64),
        'conai' => array('type' => self::TYPE_FLOAT),
        'pallet' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
        'sconto_pallet' => array('type' => self::TYPE_FLOAT),
        'quantity_per_pack' => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
        'ean13' => array('type' => self::TYPE_STRING, 'validate' => 'isEan13', 'size' => 13),
        'isbn' => array('type' => self::TYPE_STRING, 'validate' => 'isIsbn', 'size' => 32),
        'upc' => array('type' => self::TYPE_STRING, 'validate' => 'isUpc', 'size' => 12),
        'quantity' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'size' => 10),
        'reference' => array('type' => self::TYPE_STRING, 'size' => 64),
        'combination_name' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 255),
        'supplier_reference' => array('type' => self::TYPE_STRING, 'size' => 64),

        /* Shop fields */
        'wholesale_price' => array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice', 'size' => 27),
        'price' => array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isNegativePrice', 'size' => 20),
        'ecotax' => array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice', 'size' => 20),
        'weight' => array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isFloat'),
        'unit_price_impact' => array('type' => self::TYPE_FLOAT, 'shop' => true, 'validate' => 'isNegativePrice', 'size' => 20),
        'minimal_quantity' => array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isUnsignedId', 'required' => true),
        'low_stock_threshold' => array('type' => self::TYPE_INT, 'shop' => true, 'allow_null' => true, 'validate' => 'isInt'),
        'low_stock_alert' => array('type' => self::TYPE_BOOL, 'shop' => true, 'allow_null' => true, 'validate' => 'isBool'),
        'default_on' => array('type' => self::TYPE_BOOL, 'allow_null' => true, 'shop' => true, 'validate' => 'isBool'),
        'available_date' => array('type' => self::TYPE_DATE, 'shop' => true, 'validate' => 'isDateFormat'),
      ),
    );

    /**
     * @return double
     */
    public function getConai()
    {
        return $this->conai;
    }
}
